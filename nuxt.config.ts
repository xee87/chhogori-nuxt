// https://nuxt.com/docs/api/configuration/nuxt-config

import {rawLoaderPlugin} from "./inc/vite-plugin-svg-loader"

export default defineNuxtConfig({
    ssr: true,
    css: [
        'bootstrap/dist/css/bootstrap.min.css',
        'vue-final-modal/style.css',
        '~/assets/css/style.css',
        '~/assets/css/custom.css',
    ],
    runtimeConfig: {
        public: {
            apiBaseUrl: process.env.API_BASE_URL || "http://127.0.0.1:8000/api/v1/",
            siteName: process.env.SITE_NAME || "Chhogori",
            siteTitle: process.env.SITE_TITLE || "Above the noise",
            currency: process.env.CURRENCY || "$.",
            paypalClientId: process.env.PAYPAL_CLIENT_ID || "",
            social: {
                facebook: process.env.SOCIAL_FACEBOOK || "https://facebook.com",
                linkedin: process.env.SOCIAL_LINKEDIN || "https://linkedin.com",
                instagram: process.env.SOCIAL_INSTAGRAM || "https://instagram.com",
                twitter: process.env.SOCIAL_TWITTER || "https://twitter.com",
                youtube: process.env.SOCIAL_YOUTUBE || "https://youtube.com",
                pinterest: process.env.SOCIAL_PINTEREST || "https://pinterest.com",
            }
        }
    },
    components: [
        {
            path: '~/components/',
            extensions: ['.vue'],
        },
    ],
    imports: {
        dirs: [
            'stores'
        ],
    },
    vite: {
        appType: 'custom',
        ssr: {
            noExternal: [
                "moment"
            ],
        },
        plugins: [
            rawLoaderPlugin()
        ]
    },
    modules: [
        '@formkit/nuxt',
        '@nuxt-alt/auth',
        [
            '@pinia/nuxt',
            {
                autoImports: [
                    'defineStore',
                    ['defineStore', 'definePiniaStore']
                ],
            },
        ],
    ],

    auth: {
        strategies: {
            localStorage: false,
            local: {
                token: {
                    property: 'access_token',
                    global: true,
                    required: true,
                    type: 'Bearer',
                },
                endpoints: {
                    csrf: false,
                    login: {
                        url: process.env.API_BASE_URL + 'auth/login',
                        method: 'post'
                    },
                    user: {
                        url: process.env.API_BASE_URL + 'auth/user',
                        method: 'get'
                    },
                    register: {
                        url: process.env.API_BASE_URL + 'auth/register',
                        method: 'post'
                    },
                    logout: {
                        url: process.env.API_BASE_URL + 'auth/logout',
                        method: 'post'
                    }
                },
                user: {
                    property: false,
                    autoFetch: true
                }
            },
        }
    },

    app: {
        head: {
            title: "Chhogori",
            meta: [
                {charset: "utf-8"},
                {name: "viewport", content: "width=device-width, initial-scale=1"},
                {hid: "description", name: "description", content: "Chhogori"},
                {name: "format-detection", content: "telephone=no"}
            ],
            link: [
                {
                    rel: "icon",
                    type: "image/png",
                    href: "/favicon.png"
                },
                {
                    rel: "stylesheet",
                    type: "text/css",
                    href: "https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
                },
                {
                    rel: "stylesheet",
                    type: "text/css",
                    href: "https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css"
                }
            ],
        },
    }
})
