import {useNuxtApp, computed} from "#imports";
import {defineStore} from "pinia";
import helpers from "~/mixins/HelperMixin";

export const useCountriesStore = defineStore('countries', () =>
{
    const nuxtApp = useNuxtApp();

    const dataAll = ref(null);

    const all = computed( async () => {
        let countries = nuxtApp.$getStorage('countries');

        if(countries) return countries;

        if(!dataAll.value)
            dataAll.value = await load();

        nuxtApp.$setStorage('countries', dataAll.value)

        return dataAll;
    });

    async function load() {
        let response = await nuxtApp.$countriesService.all();
        if (response.success) {
            return response.data;
        }
        return null;
    }

    return { all }

});