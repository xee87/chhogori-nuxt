import {defineStore} from "pinia";

export const useGlobalStore = defineStore('global', () =>
{
    const loader = ref({
        show: false,
    });

    return { loader }

});