import {useNuxtApp, computed} from "#imports";
import {defineStore} from "pinia";
import helpers from "~/mixins/HelperMixin";

export const useSubscriptionPlansStore = defineStore('subscription-plan', () =>
{
    const nuxtApp = useNuxtApp();

    const dataSingles = ref([]);

    const dataAll = ref(null);

    const all = computed( async () => {

        let plans = nuxtApp.$getStorage('subscription_plans');

        if(plans) return plans;

        if(!dataAll.value)
            dataAll.value = await load();

        nuxtApp.$setStorage('subscription_plans', dataAll.value)

        return dataAll;

    });

    async function load() {
        let response = await nuxtApp.$subscriptionPlansService.all();
        if (response.success) {
            return response.data;
        }
        return null;
    }

    async function single(slug) {
        if(!dataSingles.value[slug]) {
            let response = await nuxtApp.$subscriptionPlansService.single(slug);

            if (response.success) {
                dataSingles.value[slug] = response.data;
            } else {
                return null;
            }
        }

        return dataSingles.value[slug];
    }

    return { all, single }

});