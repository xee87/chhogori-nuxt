import {useNuxtApp, computed} from "#imports";
import {defineStore} from "pinia";
import helpers from "~/mixins/HelperMixin";

export const useStationsStore = defineStore('stations', () =>
{
    const nuxtApp = useNuxtApp();

    const dataAll = ref(null);

    const byUser = computed( async () => {

      //  let stations = nuxtApp.$getStorage('stations');

      //  if(stations) return stations;

        if(!dataAll.value)
            dataAll.value = await load();

        return dataAll;
    });

    async function load() {
        let response = await nuxtApp.$stationsService.byUser();
        if (response.success) {
           // nuxtApp.$setStorage('stations', response.data)
            dataAll.value = response.data;
            return response.data;
        }
        return null;
    }

    async function single(id) {
        const {$stationsService} = useNuxtApp();

        let response = await $stationsService.single(id);

        if (response.success) {
            return response.data;
        }

        return null;
    }

    return { byUser, dataAll, load, single }

});