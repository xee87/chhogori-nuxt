import {defineStore} from "pinia";
import {computed, useNuxtApp} from "#imports";

export const useTrackStore = defineStore('tracks', () =>
{
    const nuxtApp = useNuxtApp();

    const uploadStep = ref(1);

    const likeCounter = ref(0);
    const followCounter = ref(0);
    const shareCounter = ref(0);

    const filteredTracks = ref([])

    const audioFile = ref({});
    const imageFile = ref({});

    const filterForm = ref({
        genre: '',
        tags: [],
        sort: '',
        free: false
    })

    const object = {
        audio: {},
        image: {},
        title: null,
        genre: null,
        tags: [],
        description: null,
        type: 1,
        additional_payment: 0,
        charity_contribution: 0,
        amount: 0,
        charity_details: {
            name: '',
            account: '',
            amount: 0
        },
        station_id: null
    };

    const trackField = ref(object);

    const byUser = ref([]);

    function setTrackFields(track){
        trackField.value = {
            audio: {},
            image: {},
            title: track.title,
            genre: track.genre_id,
            tags: track.tags,
            description: track.description,
            type: track.is_solo,
            additional_payment: track.additional_payment,
            charity_contribution: track.charity_contribution,
            amount: track.amount,
            charity_details: {
                name: track.charity_details.name,
                account: track.charity_details.account,
                amount: track.charity_details.amount
            },
            station_id: track.station_id
        };
    }

    function resetFields(){
        trackField.value = object;
    }

    function setLikeCounter(counter){
        likeCounter.value = counter;
    }

    function setFollowCounter(counter){
        followCounter.value = counter;
    }

    function setShareCounter(counter){
        shareCounter.value = counter;
    }

    function incrementLike(){
        likeCounter.value++;
    }

    function incrementFollow(){
        followCounter.value++;
    }

    function incrementShare(){
        shareCounter.value++;
    }

    function decrementLike(){
        likeCounter.value--;
    }

    function decrementFollow(){
        followCounter.value--;
    }

    function decrementShare(){
        shareCounter.value--;
    }


    async function loadByUser() {
        const {$tracksService} = useNuxtApp();
        let response = await $tracksService.byUser();
        if (response.success) {
        //    nuxtApp.$setStorage('byUserTracks', response.data)
            byUser.value = response.data;
            return response.data;
        }
        return null;
    }

    async function single(id) {
        const {$tracksService} = useNuxtApp();

        let response = await $tracksService.single(id);

        if (response.success) {
            return response.data;
        }

        return null;
    }

    return {
        trackField,
        uploadStep,
        loadByUser,
        audioFile,
        imageFile,
        single,
        likeCounter,
        followCounter,
        incrementLike,
        incrementFollow,
        incrementShare,
        decrementLike,
        decrementFollow,
        decrementShare,
        setLikeCounter,
        setFollowCounter,
        setShareCounter,
        setTrackFields,
        resetFields,
        filteredTracks,
        filterForm
    }

});