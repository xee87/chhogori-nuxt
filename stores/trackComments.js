import _ from "lodash";
import {useNuxtApp} from "#imports";
import {defineStore} from "pinia";

export const useTrackCommentsStore = defineStore('track-comments-store', () => {
    const {$trackCommentsService} = useNuxtApp();

    const commentsData = ref([]);
    const commentsCounter = ref(0);
    const likeCounter = ref(0);

    function updateCommentCounter(counter){
        commentsCounter.value +=  counter;
    }

    function setLikeCounter(counter){
        likeCounter.value = counter;
    }

    function incrementLike(){
        likeCounter.value++;
    }

    function decrementLike(){
        likeCounter.value--;
    }

    const byTrackId = async (id, page = 1) => {
        if (!commentsData.value[id]) {
            let response = await $trackCommentsService.byTrackId(id, page);

            if (response.success) {
                commentsData.value[id] = response.data;
            }
        }

        return commentsData.value[id];
    };

    function getByTrack(id){
        return commentsData.value[id] ?? [];
    }

    //

    // getNextPageBySlug: (state, getters) => (slug) => {
    //     let comments = getters.getCommentsBySlug(slug);
    //     if (comments != null) {
    //         return comments.meta.current_page || 1;
    //     }
    //     return 1;
    // }

    function deleteComment(info){
        let comments = getByTrack(info.track_id)
        if (comments != null) {
            comments = _.cloneDeep(comments);
            comments = comments.filter(comment => comment.id != info.comment_id);
            comments.forEach(comment => {
                comment.children = comment.children.filter(comment => comment.id != info.comment_id);
                comment.children.forEach(child => {
                    child.children = child.children.filter(c_comment => c_comment.id != info.comment_id);
                });
            });
            commentsCounter.value--;
            updateComment({track_id: info.track_id, data: comments});
        }
    }

    function updateComment(comment)
    {
      //  let comments = commentsData.value[comment.track_id] || [];
        commentsData.value[comment.track_id] = comment.data;
    }

    function addNewComment (info) {
        let comments = getByTrack(info.track_id);
        if (comments != null) {
            comments = _.cloneDeep(comments);
            if (info.parent) {
                comments.forEach(comment => {
                    if (comment.id == info.parent) {
                        comment.children.unshift(info.data);
                    } else {
                        comment.children.forEach(subComment => {
                            if (subComment.id == info.parent) {
                                subComment.children.unshift(info.data);
                            }
                        });
                    }
                });
            } else {
                comments.unshift(info.data);
            }
        }
        //updateCommentCounter(1)
        updateComment({track_id: info.track_id, data: comments});
    }

    return { byTrackId, addNewComment, updateComment, deleteComment, getByTrack, commentsCounter, updateCommentCounter, setLikeCounter, likeCounter, incrementLike, decrementLike  };

});