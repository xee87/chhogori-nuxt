import {defineStore} from "pinia";
import {useNuxtApp} from "#imports";

export const useGlobalStore = defineStore('auth-store', () =>
{
    const isLoggedIn = ref(false);

    return { isLoggedIn }

});