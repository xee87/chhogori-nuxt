import {defineStore} from "pinia";
import {useNuxtApp} from "#imports";

export const useProfileStore = defineStore('profile', () =>
{
    const nuxtApp = useNuxtApp();
    const inputField = ref({
        profile: {},
        cover: {}
    });

    const dataSingles = ref([]);
    const userProfile = ref({});

    async function fetchProfile(){
       // if(!userProfile.value.length){
            let response = await nuxtApp.$profileService.profile();

            if (response.success) {
                userProfile.value = response.data;
            } else {
                return null;
            }
       // }

        return userProfile.value;
    }

    async function single(username) {

        // let profiles = nuxtApp.$getStorage('profiles');
        //
        // if(profiles && profiles[username]){
        //     return profiles[username];
        // }

        if(!dataSingles.value[username]) {
            let response = await nuxtApp.$profileService.single(username);

            if (response.success) {
                dataSingles.value[username] = response.data;
            } else {
                return null;
            }
        }

        //nuxtApp.$setStorage('profiles', dataSingles.value)

        return dataSingles.value[username];
    }

    return { inputField, single, fetchProfile }

});