import {useNuxtApp, computed} from "#imports";
import {defineStore} from "pinia";
import helpers from "~/mixins/HelperMixin";

export const useFaqsStore = defineStore('faqs', () =>
{
    const nuxtApp = useNuxtApp();

    const dataAll = ref(null);

    const all = computed( async () => {
        let faqs = nuxtApp.$getStorage('faqs');

        if(faqs) return faqs;

        if(!dataAll.value)
            dataAll.value = await load();

        nuxtApp.$setStorage('faqs', dataAll.value)

        return dataAll;
    });

    async function load() {
        let response = await nuxtApp.$faqsService.all();
        if (response.success) {
            return response.data;
        }
        return null;
    }

    return { all }

});