import {useNuxtApp, computed} from "#imports";
import {defineStore} from "pinia";
import helpers from "~/mixins/HelperMixin";

export const useGenresStore = defineStore('genres', () =>
{
    const nuxtApp = useNuxtApp();

    const dataAll = ref(null);

    const selectedGenres = ref([])

    const all = computed( async () => {
        // let genres = nuxtApp.$getStorage('genres');
        //
        // if(genres) return genres;

        if(!dataAll.value)
            dataAll.value = await load();

        //nuxtApp.$setStorage('genres', dataAll.value)

        return dataAll.value;
    });

    async function load() {
        let response = await nuxtApp.$genresService.all();
        if (response.success) {
            return response.data;
        }
        return null;
    }

    return { all, selectedGenres }

});