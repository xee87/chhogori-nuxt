import { defineNuxtPlugin } from "#imports";
import helperMixin from '~/mixins/HelperMixin'

export default defineNuxtPlugin((nuxtApp) => {
    if(!nuxtApp.vueApp.__helperMixin__) {
        nuxtApp.vueApp.__helperMixin__ = true;
        nuxtApp.vueApp.mixin(helperMixin);
    }
})