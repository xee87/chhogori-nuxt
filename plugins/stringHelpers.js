import {defineNuxtPlugin} from "#imports";

export default defineNuxtPlugin((NuxtApp) =>
{
    return {
        provide: {
            ucFirst: (string) => {
                return string.charAt(0).toUpperCase() + string.slice(1);
            },
        }
    };
})