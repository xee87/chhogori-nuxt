import {defineNuxtPlugin, useNuxtApp} from "#imports";

export default defineNuxtPlugin((nuxtApp) =>
{

    function setStorage(key, value, hours = 4) { //ttl: 4hrs
        let ttl = hours *60*60*1000;
        const item = {
            value: value,
            expiry: Date.now() + ttl,
        }
        localStorage.setItem(key, JSON.stringify(item))
    }

    function getStorage(key) {
        const itemStr = localStorage.getItem(key)

        // if the item doesn't exist, return null
        if (!itemStr) {
            return null
        }
        const item = JSON.parse(itemStr)
        // compare the expiry time of the item with the current time
        if (item.expiry && Date.now() > item.expiry) {
            // If the item is expired, delete the item from storage
            // and return null
            localStorage.removeItem(key)
            return null
        }
        return item.value
    }

    function removeStorage(key){
        localStorage.removeItem(key)
    }

    return {
        provide: {
            setStorage,
            getStorage,
            removeStorage
        }
    }
})
