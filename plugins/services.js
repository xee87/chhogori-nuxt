import { defineNuxtPlugin } from "#imports";
import AuthService from "~/services/AuthService";
import SubscriptionPlansService from "~/services/SubscriptionPlansService";
import FaqsService from "~/services/FaqsService";
import CountriesService from "~/services/CountriesService";
import ProfileService from "~/services/ProfileService";
import UserSubscriptionsService from "~/services/UserSubscriptionsService";
import GenresService from "~/services/GenresService";
import StationsService from "~/services/StationsService";
import TracksService from "~/services/TracksService";
import TrackCommentsService from "~/services/TrackCommentsService";
import SubscribersService from "~/services/SubscribersService";
import PlaylistsService from "~/services/PlaylistsService";
import StreamsService from "~/services/StreamsService";

export default defineNuxtPlugin((nuxtApp) =>
{
    const authService = new AuthService(nuxtApp);
    const subscriptionPlansService = new SubscriptionPlansService(nuxtApp);
    const faqsService = new FaqsService(nuxtApp);
    const countriesService = new CountriesService(nuxtApp);
    const profileService = new ProfileService(nuxtApp);
    const userSubscriptionsService = new UserSubscriptionsService(nuxtApp);
    const genresService = new GenresService(nuxtApp);
    const stationsService = new StationsService(nuxtApp);
    const tracksService = new TracksService(nuxtApp);
    const trackCommentsService = new TrackCommentsService(nuxtApp);
    const subscribersService = new SubscribersService(nuxtApp);
    const playlistsService = new PlaylistsService(nuxtApp);
    const streamsService = new StreamsService(nuxtApp);
    return {
        provide: {
            authService,
            subscriptionPlansService,
            faqsService,
            countriesService,
            profileService,
            userSubscriptionsService,
            genresService,
            stationsService,
            tracksService,
            trackCommentsService,
            subscribersService,
            playlistsService,
            streamsService
        }
    }
})
