import { createVfm } from 'vue-final-modal'
import {defineNuxtPlugin} from "#imports";
import { ModalsContainer } from 'vue-final-modal'
export default defineNuxtPlugin((nuxtApp) => {
    if(!nuxtApp.vueApp.__vfModalUse__) {
        nuxtApp.vueApp.__vfModalUse__ = true;
        const vfm = createVfm();
        nuxtApp.vueApp.use(vfm)
    }
    nuxtApp.vueApp.component('ModalsContainer', ModalsContainer);
})