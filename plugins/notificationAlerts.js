import {defineNuxtPlugin, useNuxtApp} from "#imports";
import Notifications from "@kyvg/vue3-notification";
import {useNotification} from "@kyvg/vue3-notification";
import helpers from "~/mixins/HelperMixin";

export default defineNuxtPlugin((nuxtApp) =>
{
    if(!nuxtApp.vueApp.__notifyUse__) {
        nuxtApp.vueApp.__notifyUse__ = true;
        nuxtApp.vueApp.use(Notifications);
    }

    const notification = (text, type = "success", title="") => {
        const {notify} = useNotification()
        if(!title) title = helpers.methods.UCFirst(type);
        notify({
            title,
            type,
            text
        });
    };

    return {
        provide: {
            notify: notification
        }
    }
})
