import { defineNuxtPlugin } from "#imports";
import { Carousel, Slide, Navigation, Pagination } from 'vue3-carousel';
import 'vue3-carousel/dist/carousel.css';
import { Collapse } from 'vue-collapsed'
import Vue3TagsInput from 'vue3-tags-input';

export default defineNuxtPlugin((nuxtApp) => {
    nuxtApp.vueApp.component('Carousel', Carousel);
    nuxtApp.vueApp.component('Slide', Slide);
    nuxtApp.vueApp.component('Navigation', Navigation);
    nuxtApp.vueApp.component('Pagination', Pagination);

    //vue-collapse
    nuxtApp.vueApp.component('Collapse', Collapse);

    //vue-tags-input
    nuxtApp.vueApp.component('Vue3TagsInput', Vue3TagsInput);

})