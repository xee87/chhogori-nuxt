import {defineNuxtPlugin} from "#imports";
import { loadScript } from "@paypal/paypal-js";

export default defineNuxtPlugin((nuxtApp) =>
{
    async function loadPaypal(){
       return await loadScript({
           "client-id": nuxtApp.$config.public.paypalClientId,
           'vault': true,
           'intent':"subscription"
       });
    }

    return {
        provide: {
            loadPaypal
        }
    }
})
