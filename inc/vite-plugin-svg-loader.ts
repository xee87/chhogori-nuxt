import {dataToEsm} from "@rollup/pluginutils";
import fs from "fs";

export const rawLoaderPlugin = () => {
    return {
        name: "raw-file-loader",
        transform (_: string, filepath: string) {
            if (filepath.includes("node_modules")) {
                return null;
            }
            if (filepath.endsWith(".svg")) {
                return {
                    code: dataToEsm(fs.readFileSync(filepath).toString())
                };
            }
        }
    }
}