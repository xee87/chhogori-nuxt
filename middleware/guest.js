import {defineNuxtRouteMiddleware, navigateTo, abortNavigation, useNuxtApp} from "#imports";

export default defineNuxtRouteMiddleware((to, from) => {
    const {$auth} = useNuxtApp();
    if (!$auth.loggedIn) {
        return
    }
    return navigateTo('/buy-plans')
})