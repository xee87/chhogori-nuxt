import { generateClasses } from '@formkit/themes'

const isCheckboxAndRadioMultiple = (node) => (node.props.type === 'checkbox' || node.props.type === 'radio') && node.props.options;

function autoPropsFromIdPlugin (node) {
    const title = makeTitle(node.name ? node.name : node.props.id);

    if(node.props.type === 'form') {
        if(!node.props.incompleteMessage)
            node.props.incompleteMessage = 'Sorry, not all the fields are filled out correctly.';
    }

    //if(!node.name)
    //    node.name = node.props.id;

    if(!node.props.label)
        node.props.label = title;

    /*if (!['button', 'submit'].includes(node.props.type)) {
        // automatically set help text, but exclude buttons
        node.props.help = 'Please enter your ' + title.toLowerCase() + '.';
        node.placeholder = 'Please enter ' + title.toLowerCase() + '.';
    }*/

    if(node.props.parsedRules.some(rule => rule.name === 'required')) {
        if (isCheckboxAndRadioMultiple(node)) {
            node.props.legandClass = 'required';
        } else {
            node.props.labelClass = 'required';
        }
    }
}

function makeTitle (slug) {
    if (slug === undefined) return "";

    let words = slug.split(/[-_]+/);

    for (let i = 0; i < words.length; i++) {
        let word = words[i];
        words[i] = word.charAt(0).toUpperCase() + word.slice(1)
    }

    return words.join(' ');
}

const inputClasses = 'border w-100 mt-md-0 hero-input rounded bg-white text-secondary ';

const config = {
    config: {
        classes: generateClasses({
            global: { // classes
                outer: '$reset mb-3 form-group',
                label: 'form-label',
                help: 'form-text',
                messages: 'text-danger mb-0 list-unstyled'
            },
            form: {
                form: ""
            },
            text: {
                input: inputClasses,
            },
            radio: {
                legend: 'form-label'
            },
            textarea: {
                input: inputClasses,
            },
            email: {
                input: inputClasses,
            },
            select: {
                input: inputClasses,
            },
            password:{
                input: inputClasses
            },
            checkbox: {
                wrapper: 'form-check cursor-pointer',
                inner: 'mr-2',
                input: 'form-check-input',
                label: 'font-weight-normal fs-14 text-theme-secondary form-check-label'
            },
            range: {
                input: '$reset form-range',
            },
            submit: {
                outer: '$reset mt-3',
                input: '$reset btn btn-primary'
            }
        })
    },
    plugins: [
        autoPropsFromIdPlugin,
    ],
}

export default config
