import moment from "moment";
import {useNuxtApp} from "#imports";
import {useGlobalStore} from "~/stores/global";

export default {

    methods: {

        /**
         * @return {string}
         */
        ShortenText(text, length, addEllipsis) {
            length = length || 50;

            if(addEllipsis === undefined)
                addEllipsis = true;

            if (!text || text.length <= length) return text;

            return text.substr(0, length) + (addEllipsis ? '…' : '');
        },

        dateFromNow(date) {
            return moment(date).fromNow();
        },

        /**
         * @return {number}
         */
        DurationInDays(start_date, end_date) {
            return Math.floor(
                moment.duration(
                    moment(start_date).diff(
                        moment(end_date)
                    )
                )
                .asDays()
            );
        },

        /**
         * @return {string}
         */
        FormatCurrency(number) {
            //const config = useRuntimeConfig();
            if (number) {
                return /*config.public.currency + */"$" + number.toLocaleString();
            }
            return /*config.public.currency +*/ "$0";
        },

        /**
         * @return {string}
         */
        FormatDate(date, format = "DD MMM, YYYY") {
            let dateObj = moment(date);
            return dateObj.format(format);
        },

        filterArray(array, key, value, compare = "!=") {
            if (compare === "!=") {
                return array.filter(data => data[key] !== value);
            }
            return array.filter(data => data[key] === value);
        },

        /**
         * @return {string}
         */
        FormatCount(num, digits = 1) {
            const lookup = [
                {value: 1, symbol: ""},
                {value: 1e3, symbol: "k"},
                {value: 1e6, symbol: "M"},
                {value: 1e9, symbol: "G"},
                {value: 1e12, symbol: "T"},
                {value: 1e15, symbol: "P"},
                {value: 1e18, symbol: "E"}
            ];
            const rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
            let item = lookup.slice().reverse().find(function (item) {
                return num >= item.value;
            });
            return item ? (num / item.value).toFixed(digits).replace(rx, "$1") + item.symbol : "0";
        },

        getDateDaysFromNow(date) {
            return moment.duration(Date.parse(date) - Date.now()).days();
        },

        getRandomFromArray(arr, num) {
            const shuffled = [...arr].sort(() => 0.5 - Math.random());
            return shuffled.slice(0, num);
        },

        /**
         * @return {number}
         */
        GetRandomNumber(min, max){
            min = min || 1;
            max = max || 10;

            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min + 1)) + min;
        },

        Loader(show) {
            const { loader } = useGlobalStore();

            loader.show = show;
        },

        BodyOverflow(hidden) {
            hidden === true ?
                document.querySelector("body").classList.add("overflow-hidden") :
                document.querySelector("body").classList.remove("overflow-hidden");
        },

        /**
         * @return {string}
         */
        UCFirst(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        },

    }
};

