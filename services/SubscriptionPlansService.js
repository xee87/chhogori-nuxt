import ApplicationService from "~/services/ApplicationService";

export default class SubscriptionPlansService extends ApplicationService {

	single(plan_id){
		return this.get("subscription-plans/single/" + plan_id);
	}

	all(){
		 return this.get("subscription-plans/all");
	}

}
