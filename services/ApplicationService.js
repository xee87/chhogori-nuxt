import _ from 'lodash'
import {useGlobalStore} from "~/stores/global";
import {useNuxtApp} from "#imports";
import axios from "axios";
import extend from "lodash/extend";

class ApplicationService {

    constructor(nuxtApp) {
        this.nuxtApp = nuxtApp;
    }

    async get(path, data = {}) {
        return await this.request(path, 'GET', data)
    }

    async post(path, data, isFile = false) {
        return await this.request(path, 'POST', data, isFile)
    }

    async put(path, data, isFile = false) {
        return await this.request(path, 'PUT', data, isFile)
    }

    async patch(path, data, isFile = false) {
        return await this.request(path, 'PATCH', data, isFile)
    }

    async delete(path, data = {}) {
        return await this.request(path, 'DELETE', data)
    }

    pagination(page = 1) {
        return page > 1 ? '?page=' + page : ''
    }

    makeResponse(response) {
        let output = {
            success: true,
            status: response.status,
            type: "",
            data: {},
            meta: {}
        };
        let status = response.status;
        switch (true) {
            case (status >= 500 && status < 600):
                output.success = false;
                output.data = response.data.errors || [response.data.message];
                break;
            case (status >= 400 && status < 500):
                output.success = false;
                output.data = response.data.errors || [response.data.message];
                break;
            case  (status >= 200 && status < 300):
                const data = (response && response.data) || {};
                output.data = (data && data.data) || {};
                output.meta = (data && data.meta) || {};
                output.type = (data && data.meta) || "";
        }
        return output;
    }

    toFormData(object) {
        const formData = new FormData();
        Object.keys(object).forEach(key => formData.append(key, object[key]));
        return formData;
    }

    async request(path, method = "GET", paramsData = {}, isFile = false) {
        const {loader} = useGlobalStore();
        const {$auth, $config} = useNuxtApp()
        loader.show = true;

        let extraConfig = {
            headers: {
               // 'Access-Control-Allow-Origin': '*',
            }
        };
        if (isFile) {
            extraConfig.headers = {
                "Content-Type": "multipart/form-data"
            };
            paramsData['_method'] = method;
            if(method === 'PUT' || method === 'PATCH'){
                method = 'POST';
            }
            paramsData = this.toFormData(paramsData);
        }
        const config = extend(
            {
                method,
                url: $config.public.apiBaseUrl + path,
                responseType: "json",
                data: paramsData
            },
            extraConfig
        );

        if($auth.loggedIn){
            extraConfig['headers']['Authorization'] = $auth.strategy.token.get()
        }

        try {
            let response = await axios(config);
            loader.show = false;
            return this.makeResponse(response);
        } catch (e) {
            console.log(e.response, 'in applicaton service');
            loader.show = false;
            return this.makeResponse(e.response);
        }
    }
}

export default ApplicationService
