import ApplicationService from "~/services/ApplicationService";

export default class TracksService extends ApplicationService {

	byUser(){
		return this.get("tracks/byUser");
	}

	latest(){
		return this.get("tracks/latest");
	}

	espForYou(){
		return this.get("tracks/espForYou");
	}

	newTrackByGenre(){
		return this.get("tracks/newTrackByGenre");
	}

	byGenre(genre_id){
		return this.get("tracks/byGenre/"+genre_id);
	}

	single(track_id){
		return this.get("tracks/single/" + track_id);
	}

	filter(
		genre,
		tags,
		sort,
		free,
		page = 1,
	){
		let params = '?genre='+genre+'&tags='+tags+'&sort='+sort+'&free='+free+'&page='+page;
		return this.get("tracks/filter" + params);
	}

	create(
		audio,
		image,
		title,
		description,
		genre_id,
		tags,
		type = 1,
		additional_payment = 0,
		charity_contribution = 0,
		amount = 0,
		charity_details = null,
		station_id = null
	){
		 return this.post(
			 "tracks/create",
			 {
				 audio,
				 image,
				 title,
				 description,
				 genre_id,
				 tags,
				 type,
				 additional_payment,
				 charity_contribution,
				 amount,
				 charity_details,
				 station_id
			 },
			 true
		 );
	}

	update(
		track_id,
		audio,
		image,
		title,
		description,
		genre_id,
		tags,
		type = 1,
		additional_payment = 0,
		charity_contribution = 0,
		amount = 0,
		charity_details = null,
		station_id = null
	){
		return this.patch(
			"tracks/update/" + track_id,
			{
				audio,
				image,
				title,
				description,
				genre_id,
				tags,
				type,
				additional_payment,
				charity_contribution,
				amount,
				charity_details,
				station_id
			},
			true
		);
	}

	deleteTrack(track_id){
		return this.delete("tracks/delete/"+track_id);
	}

	follow(track_id){
		return this.get("tracks/follow/"+track_id);
	}

	unfollow(track_id){
		return this.get("tracks/unfollow/"+track_id);
	}

	like(track_id){
		return this.get("tracks/like/"+track_id);
	}

	unlike(track_id){
		return this.get("tracks/unlike/"+track_id);
	}

}
