import ApplicationService from "~/services/ApplicationService";

export default class StationsService extends ApplicationService {

	byUser(){
		 return this.get("stations/byUser");
	}

	latest(){
		return this.get("stations/latest");
	}

	espForYou(){
		return this.get("stations/espForYou");
	}

	newTrackByGenre(){
		return this.get("stations/newTrackByGenre");
	}

	byGenre(genre_id){
		return this.get("stations/byGenre/"+genre_id);
	}

	single(station_id){
		return this.get("stations/single/" + station_id);
	}

	create(
		image,
		title,
		description,
		genre_id,
		tags,
	){
		return this.post(
			"stations/create",
			{
				image: image,
				title,
				description,
				genre_id,
				tags,
			},
			true
		);
	}

	update(
		station_id,
		image,
		title,
		description,
		genre_id,
		tags,
	){
		return this.patch(
			"stations/update/" + station_id,
			{
				image: image,
				title,
				description,
				genre_id,
				tags,
			},
			true
		);
	}

	deleteStation(station_id){
		return this.delete("stations/delete/"+station_id);
	}

}
