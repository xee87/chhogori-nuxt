import ApplicationService from "~/services/ApplicationService";

export default class PlaylistsService extends ApplicationService {

	create(){
		 return this.post("playlists/create");
	}

	addTrack(playlist_id, track_id){
		return this.post("playlists/add-track/" + playlist_id + "/" + track_id);
	}

}
