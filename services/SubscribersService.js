import ApplicationService from "~/services/ApplicationService";

export default class SubscribersService extends ApplicationService {

	create(
		email,
	){
		return this.post(
			"subscriber/create",
			{
				email,
			}
		);
	}

}
