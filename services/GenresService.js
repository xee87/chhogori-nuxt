import ApplicationService from "~/services/ApplicationService";

export default class GenresService extends ApplicationService {

	all(){
		 return this.get("genres/all");
	}

	random(){
		return this.get("genres/random");
	}

}
