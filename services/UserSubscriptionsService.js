import ApplicationService from "~/services/ApplicationService";

export default class UserSubscriptionsService extends ApplicationService {

	add(
		plan_id,
		amount,
		paypal_subscription_id,
		details,
	){
		return this.post(
			"user-subscriptions/add",
			{
				plan_id,
				amount,
				paypal_subscription_id,
				details,
			}
		);
	}

}
