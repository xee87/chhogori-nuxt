import ApplicationService from "~/services/ApplicationService";

export default class CountriesService extends ApplicationService {

	all(){
		 return this.get("countries/all");
	}

}
