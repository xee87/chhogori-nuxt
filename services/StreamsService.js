import ApplicationService from "~/services/ApplicationService";

export default class StreamsService extends ApplicationService {

	all(){
		 return this.get("streams/all");
	}
}
