import ApplicationService from "./ApplicationService";

export default class TrackCommentsService extends ApplicationService {

	byTrackId(id, page = 1) {
		return this.get("track-comments/" + id + this.pagination(page));
	}

	new(id, comment, parent_comment = null){
		return this.post(
			"track-comments/" + id,
			{
				comment,
				parent_comment
			}
		);
	}

	// Reasons:
	// Abuse = 1
	// Harassment = 2
	// Rules Violation = 3
	report(comment_id, reason, message){
		return this.post(
			"track-comments/report/" + comment_id,
			{
				reason,
				message
			}
		);
	}

	deleteComment(comment_id){
		return this.delete("track-comments/" + comment_id);
	}

	like(track_id){
		return this.get("track-comments/like/"+track_id);o
	}

	unlike(track_id){
		return this.get("track-comments/unlike/"+track_id);
	}

}
