import ApplicationService from "./ApplicationService";

export default class AuthService extends ApplicationService {

	login(email, password) {
		return this.post(
			"auth/login",
			{email, password}
		);
	}

	register(username, email, password) {
		return this.post(
			"auth/register",
			{email, username, password}
		);
	}

	forgotPassword(email){
		return this.post(
			"auth/password/forgot-password",
			{email}
		);
	}

	resetPassword(token, email, password, password_confirmation){
		return this.post(
			"auth/password/reset",
			{email,token,password,password_confirmation}
		);
	}

	emailExists(email) {
		return this.post(
			"auth/exists/email",
			{email}
		);
	}

	usernameExists(username) {
		return this.post(
			"auth/exists/username",
			{username}
		);
	}

}
