import ApplicationService from "~/services/ApplicationService";

export default class FaqsService extends ApplicationService {

	all(){
		 return this.get("faqs/all");
	}

}
