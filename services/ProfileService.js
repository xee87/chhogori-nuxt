import ApplicationService from "~/services/ApplicationService";

export default class ProfileService extends ApplicationService {

	update(
		first_name,
		last_name,
		username,
		email,
		paypal_account,
		bio,
		country_id,
		password = null,
		profile_photo = null,
		cover_photo = null
	){
		 return this.patch(
			 "users/profile/update",
			 {
				 first_name,
				 last_name,
				 username,
				 email,
				 paypal_account,
				 bio,
				 country_id,
				 password,
				 profile_photo,
				 cover_photo
			 },
			 true
		 );
	}

	single(username){
		return this.get("users/single/" + username);
	}

	profile(){
		return this.get("users/profile/full");
	}

	authors(){
		return this.get("users/authors");
	}

	updateGenre(genres){
		return this.patch(
			"users/profile/update-genres",
			{
				genres
			}
		);
	}

	follow(user_id){
		return this.get("users/follow/"+user_id);
	}

	unfollow(user_id){
		return this.get("users/unfollow/"+user_id);
	}

}
